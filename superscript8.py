#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shlex as sh
import subprocess
import pipe_d as p

p_imagenes = '/home/bos0109/mirta1/programa/Imagenes2'
p_cod = '/home/bos0109/mirta1/programa/codigos'

f = open('/home/bos0109/mirta1/programa/resultados8_2.dat','a+')
f.close()

os.chdir(p_imagenes)

l = 'lista_directorios8.dat'

os.system('ls *_8 -d > '+l)

Z = '    '

lista = open(l, 'r')

LIST = lista.readlines()

cantidad = len(LIST)

porcion = int(cantidad/4.)


for i in range(0,cantidad):
    line = LIST[i]
    path = os.path.abspath(line.rstrip('\n'))
    os.chdir(path)
    parametros = open('archivo_parametros.txt').readlines()
    SN = int(parametros[4].split()[1])
    n = float(parametros[5].split()[2])
    gx_size = int(parametros[8].split()[2])
    gx_mabs = float(parametros[9].split()[4])
    Dl = int(parametros[10].split()[3])
    rate = int(parametros[12].split()[4])
    phi_0 = float(parametros[13].split()[4])
    r_t = float(parametros[15].split()[6])
    gx_scale = round(float(parametros[21].split()[4]))
    with open('/home/bos0109/mirta1/programa/resultados8_2.dat','a') as r:
        r.write(str(SN)+Z+str(n)+Z+str(gx_size)+Z+str(gx_mabs)+Z+str(Dl)+Z+str(rate)+Z+str(phi_0)+Z+str(r_t)+Z+str(gx_scale)+Z)
    #p.pipeline(path)
    command = 'Rscript ../../codigos/engine8.R'
    arg=sh.split(command)
    subprocess.call(arg)
    os.chdir(p_imagenes)
