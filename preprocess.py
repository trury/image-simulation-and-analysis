#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as np
import subprocess
import shlex
from astroML.crossmatch import crossmatch
from astropy.io import fits


flat = fits.open('../Flat.fits')
dark = fits.open('../Dark.fits')
bias = fits.open('../Zero.fits')

f = flat[0].data
d = dark[0].data
b = bias[0].data

#sample every how many pixels?
#------------------------------------------------------------------------------#
pix=64
lop=2*pix
axis = 4096
sze= (axis/pix)*(axis/pix)+2*(axis/pix)+1 # = (axis/pix +1)**2.
#------------------------------------------------------------------------------#

image = fits.open(image_path)
image_data = image[0].data

darkcor = 'DARKCOR' in image[0].header.keys()
flatcor = 'FLATCOR' in image[0].header.keys()
biascor = 'ZEROCOR' in image[0].header.keys()

if not biascor:
    image_data = image_data - b

if not darkcor:
    image_data = image_data - d

if not flatcor:
    image_data = image_data/f

kp=np.where(image_data > -100.) & (image_data < 25000.)
q=image_data(kp)
mmm,q,sky,sig #this finds the median sky value
sig0=sig



