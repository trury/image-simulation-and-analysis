#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as np
import astroML.time_series as Tseries
from matplotlib import pyplot as plt
#fname comes from /opt/Bruno/Imagenes/lightcurves/listcurves.dat

def lc_attack(fname):
	lcpath = '/opt/Bruno/Images/lightcurves'
	LC = genfromtxt(os.path.join(lcpath, fname), names=True, dtype=None)
	#LC is the lightcurve
	#
	#Tseries.lomb_scargle     ---  Periodogram from LScargle
	t = LC['JD_TIME']
	y = LC['MAG_ISOCOR']
	dy= LC['MAGERR_ISOCOR']
	period = 10 ** np.linspace(-1, 0, 10000)
	omega = 2. * np.pi / period
	PG = Tseries.lomb_scargle(t, y, dy, omega, generalized=True)
	

