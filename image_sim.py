#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------#
import numpy as np
import scipy as sp
import math as m
import os
import subprocess
import shlex
from scipy import signal as sg
from scipy import stats
from scipy import special as spe
from random import randint
#-------------------------------------------------------------------------------#
#lines1 = [line.strip() for line in open('Kilo_absol.dat')]
#lightcurve = np.zeros( (149,9) )
#for i in range(1,len(lines1)):
#    for j in range(0,9):
#        a = lines1[i]
#        a = a.split()
#        lightcurve[i-1,j]=float(a[j])
#-------------------------------------------------------------------------------#
def Psf(N,FWHM): #2 arguments.
    a=np.zeros((N,N))
    mu=(N-1)/2.
    sigma=FWHM/(2. * m.sqrt(2.*m.log(2.)) )
    for i in range(N-1):
        for j in range(N-1):
            a[i,j]=stats.norm.pdf(i,loc=mu,scale=sigma)*stats.norm.pdf(j,loc=mu,scale=sigma)
    return(a)
#-------------------------------------------------------------------------------#
def _airy_func(rr, width, amplitude=1.0): # 3 arguments
    return amplitude * (2.0 * sp.special.j1(rr/width) / (rr/width))**2
#-------------------------------------------------------------------------------#
def airy_patron(N,width): #2 arguments
    mu=(N-1)/2.
    a=np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            r_pix=m.sqrt((i-mu)**2 + (j-mu)**2)
            a[i,j]=_airy_func(r_pix,width)
    return(a)
#-------------------------------------------------------------------------------#
def convol_fft(gal,a): #2 arguments
    b=sg.fftconvolve(gal, a, mode="same")
    #b=sp.signal.fftconvolve(gal, a, mode="same")
    return(b)
#-------------------------------------------------------------------------------#
def pixelize(L,M): # 2 arguments
    s=np.shape(M)
    if s[1]!=s[0]:
        print("Bad array, NOT SQUARE")
        return 
    elif np.mod(s[1],L)!=0:
        print("Bad new pixel size")
        return
    else:
        ma=s[1]/L
        M2=np.zeros((ma,ma)) #initialize new matrix
        for i in range(0,s[1],L):
            for j in range(0,s[1],L):
                h=0
                for k in range(0,L-1):
                    for n in range(0,L-1):
                        h=M[i+k,j+n]+h
                M2[i/L,j/L]=h
    return(M2)
#-------------------------------------------------------------------------------#
def cielo(N,lam): #2 arguments
    x = np.random.poisson(lam,(N,N)).astype(np.float32)
    return(x)
#-------------------------------------------------------------------------------#
def mags(zero,m_ap): #3 arguments
    u=10.**( (zero-m_ap)/2.5 )
    return(u)
#-------------------------------------------------------------------------------#
def bn(n): #1 arg
    k=float(n)
    b_n=2.*k-(1./3.)+(4./405.)*(1./k) + (46./25515.)*(1./k**2.) + (131./1148175.)*(1./k**3)
    return(b_n)    
#-------------------------------------------------------------------------------#
def mu_e_par_gal(Mabs,Dl,n,R_e): #4 arguments
    b_n=bn(n)
    Mapp=Mabs+5.*( m.log10(1000000.*Dl) - 1.)
    #print(2.*m.pi*n*m.exp(b_n)*(1./b_n**(2.*n))*spe.gamma(2.*n)) #--- for debugging
    mu_e=Mapp+5.*m.log10(R_e)+2.5*m.log10( 2.*m.pi*n*m.exp(b_n)*(1./b_n**(2.*n))*spe.gamma(2.*n) )
    return(mu_e)
#-------------------------------------------------------------------------------#
#old unused function!!
def psersic2(r_e, n, r, mu_e): #4 arguments
    b_n=bn(n)
    #r should be in arcsecs
    #mu_r it's in [mag/arcsec**2]
    mu_r=mu_e+(2.5*b_n*(1/m.log(10)))+( (r/r_e)**(1/n) - 1 )
    return(mu_r)
#-------------------------------------------------------------------------------#
def gal_sersic3(N,n,size,Dl,Mabs,fov,zero): #7 arguments 
    #[size]=kpc
    #[Dl]=Mpc
    R_e=(180.*60.*60./m.pi)*(size/(10.*Dl*1000.))
    res=fov/N
    gal=np.zeros((N,N))
    c=(N-1)/2.
    m_app=Mabs+5.*(m.log10(Dl*1000000.)-1)
    print('magnitud aparente de la galaxia')
    print(m_app)
    Ltot=mags(zero,m_app)
    b_n=bn(n)
    I_e=Ltot*(1./( R_e**2. * 2. * m.pi * m.exp(b_n) * b_n**(-2.*n) * spe.gamma(2.*n) ))
    for i in range(N-1):
        for j in range(N-1):
            r_pix=m.sqrt((i-c)**2. + (j-c)**2.)
            if (r_pix*res)<=(9.*R_e):
                gal[i,j]=I_e* m.exp( -b_n * ((r_pix*res/R_e)**(1./n) - 1) ) 
            else:
                gal[i,j]=0
    return(gal)
#-------------------------------------------------------------------------------#
def incline(gal, theta):#2 arguments
    s=np.shape(gal)
    gal2=np.zeros(s)
    for i in range(s[0]):
        for j in range(s[1]):
            #define new pixel's position on frame
            x=i
            y=j*m.sin(theta) #msen(theta)
            y_new=round(y) #nearest n
            gal2[int(x),int(y_new+round((s[1]/2.)*(1-m.sin(theta))))]=gal[i,j]
    return(gal2)
#-------------------------------------------------------------------------------#
def master_frame(N,n,alpha,theta,size,Mabs,Dl,fov,zero,nstars):#9 arguments
    #function thath generates Master Frame
    res=fov/N
    theta=theta/res  #converting to pixeles
    Air=airy_patron(32,theta)
    alpha=(m.pi/180.0)*alpha
    #setting stars in field
    IM=np.zeros((N,N))
    for i in range(nstars):
        IM[randint(1,N-1),randint(1,N-1)]=mags(zero,8.+12.*(np.random.random_sample()))
    G=gal_sersic3(N,n,size,Dl,Mabs,fov,zero)
    GI=incline(G,alpha)
    #GII=mag_a_cuentas(GI,zero)
    #GII is the galaxy including zp
    M=IM+GI
    IMC=convol_fft(M,Air)
    return(IMC)
#-------------------------------------------------------------------------------#
def inyeccion(MF,JD,largo,d,phi_0,t_decay,x,y,theta,fov,zero,Dl):#12 arguments
    #JD: date, moves between d y d+largo
    #theta in arcsecs
    #phi_0:location of peak of Kilonovae (porcentage units)
    #r_t : transient distance to galaxy centre on r_e units
    #beta:angle between the major semi-axis of galaxy and the transient position
    #Mabs=-13.
    #mag_pico=Mabs-5.+5.*m.log10( Dl*1000000 )
    #print('pico transit:',mag_pico)
    s=np.shape(MF)
    N=s[0]
    res=fov/N  #plate scale
    theta=theta/res #but converted to pixeles
    Air=airy_patron(32,theta)
    dia_pico=d+(largo*phi_0)#day where starts transient
    if JD<dia_pico:
	return(MF)
    else:
#----------------------------------------------------------------
    #escala=t_decay #escala en dias de la distribucion
    #cuentas_pico=10.**((zero-mag_pico)/2.5)
    #gau=cuentas_pico*stats.norm.pdf(JD,loc=dia_pico, scale=escala)
    #mag_ap=zero-2.5*m.log10(gau)
    #print('mag_ap',mag_ap,'JD=',JD)
#-----------------------------------------------------------------
    #tiempos son ahora los valores i de lightcurve[:,0][i]
    #magnitudes en la banda g son  i de lightcurve[:,5][i]
    #magnitudes en la banda r son  i de lightcurve[:,6][i]
    #magnitudes en la banda i son  i de lightcurve[:,7][i]
    #magnitudes en la banda z son  i de lightcurve[:,8][i]
	wh = np.where(np.absolute( (JD-2456559.)-lightcurve[:,0] ) == min( np.absolute( (JD-2456559)-lightcurve[:,0])))[0][0]
	M_r=lightcurve[wh,6]
	mag_app=M_r-5.+5.*m.log10( Dl*1000000 )
    	transit=np.zeros(s)
	transit[x,y]=mags(zero,mag_app)
    	IM=convol_fft(transit,Air)
    	return(MF+IM)
#-------------------------------------------------------------------------------#
def image(MF,N2, FWHM,SN, x, y,fov,zero): #8 arguments
    #function thath generates images with noise and seeing from
    #a master frame, and pixelize it to a square size of N2 side
    #MF = image Master
    #FWHM = FWHM seeing
    #SN signal to noise relation
    #N2= final frame size, after pixelizing   
    s=np.shape(MF)
    N=s[0]
    res=fov/N
    FWHM=FWHM/res
    PSF=Psf(32,FWHM)
    #------------------------------------------------------#
    Mabs_kilo_r=lightcurve[0,6]
    Dist_media_kilo=130000000 #pc
    m_app_kilo=Mabs_kilo_r-5.+5.*m.log10( Dist_media_kilo )
    U=mags(zero,m_app_kilo)
    #------------------------------------------------------#
    lam=U/SN
    IM=convol_fft(MF,PSF)
    #IM is convolved image with both patterns
    #star with value 50*a must be of magnitud 12
    image=pixelize(N/N2,IM)
    C=cielo(N2,lam)
    F=C+image
    return(F)
#-------------------------------------------------------------------------------#
def gen1(path,FWHM,N,N2,SN,n,alpha,theta,size,Mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,nstars):
    copyp(path)
    fov=float(fov)
    Master_F=master_frame(N,n,alpha,theta,size,Mabs,Dl,fov,zp,nstars)
    print("Master Frame Listo")
    Ntot=int(b_line*rate)
    d=2456559. #JD of time series start
    #using transient parameters
    res=fov/N  #scale of the plate
    R_e=(180.*60.*60./m.pi)*(size/(10.*Dl*1000.))
    filegen1(path,FWHM,N,N2,SN,n,alpha,theta,size,Mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,Ntot,res,R_e)
    for i in range(1,Ntot+1):
        t_exp=60.
        #define random JD between c and d (no longer used)
        #JD=largo * np.random.random_sample() + d
        JD=(1./float(rate))*i + d
        print('dia juliano')
        print(JD)
        #encapsulate images on fits files
        x=int(round((r_t*R_e/res)*m.sin(beta))) + int( (float(N)-1.)/2.)
        y=int(round((r_t*R_e/res)*m.cos(beta)*m.sin(alpha))) + int( (float(N)-1.)/2.)
        MF=inyeccion(Master_F,JD,b_line,d,phi_0,t_decay,x,y,theta,fov,zp,Dl)
        A=image(MF,N2,FWHM,SN,x,y,fov,zp)
        capsule_corp(path,A,JD,t_exp,i,zp,FWHM,res*float(N2)/float(N))
    os.chdir('../../codigos')
#-------------------------------------------------------------------------------#
from astropy.io import fits
from astropy.time import Time
def capsule_corp(p1,gal, t, t_exp, i,zero,FWHM,res):
        #p="/media/F038D6A538D669DC/Users/Bruno/Documents/tesis/curvas_sinteticas/"
        #p=os.path.join(p,p1)
        path=os.path.abspath(p1)
        s=np.shape(gal)
        for l in range(0,s[0]):
            for j in range(0, s[1]):
                gal[l,j]=round(gal[l,j])
       	file1=fits.PrimaryHDU(gal)
	hdulist=fits.HDUList([file1])
	hdr=hdulist[0].header
	time=Time(t, format='jd', scale='utc')
	dia = time.iso[0:10]
	hora = time.iso[11:24]
	jd= time.jd
	hdr.set('TIME-OBS', hora)
	hdr.set('DATE-OBS', dia)
	hdr.set('EXPTIME', t_exp)
	hdr.set('JD', jd)
	hdr.set('ZERO_P',zero)
	hdr.set('PX_SCALE',res)
	hdr.set('S_FWHM',FWHM)
	path_fits=os.path.join(path,('image'+str(i).zfill(5)+'.fits'))
	hdulist.writeto(path_fits)
#-------------------------------------------------------------------------------#
#parameter file generator
def filegen1(path,FWHM,N,N2,SN,n,alpha,theta,size,Mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,Ntot,res,R_e):
    words=('Image Directory=','Seeing FWHM [arcsec]=')
    words=words+('N [pix]','N2 [pix]=','SN=','sersic index=')
    words=words+('Galaxy inclination [deg]=','Airy size [arcsec]=')
    words=words+('Galaxy size[Kpc]=','Galaxy Absolute Magnitude [mag]=')
    words=words+('Luminosity Distance [Mpc]=','Time series base line [day]=')
    words=words+('Time series rate [Images/days]=','Event Initial Phase [%]=')
    words=words+('Decay Time Series Scale [day]=','Transit Distance to Gx Center [R_e]=')
    words=words+('Position Angle Transit (over Gx Major axis) [deg]=','Phot zero=','FoV [arcsec]=')
    words=words+('Total Number of Images=','Resolution [arcsec/px]=','Gx Scale Radius [arcsec]=')
    values=(os.path.abspath(path),FWHM,N,N2,SN,n,alpha,theta,size,Mabs,Dl,b_line,rate,phi_0,t_decay,r_t,beta,zp,fov,Ntot,res,R_e)
    p2=os.path.abspath(path)
    os.chdir(p2)
#    filename=os.path.join(p2,'archivo_parametros.txt')
    filename='archivo_parametros.txt'
    f=open(filename,'w')
    for i in range(len(words)):
        W=str(values[i])
        #ruler = (words[i],W)
        f.writelines('{0:55}  {1}'.format(words[i].rjust(54),W))
        f.write('\n')
    f.close()
    return
#--------------------------------------------------------------------------------#
def copyp(path):
    path_fits=os.path.abspath(path)
    path_files='../codigos'
    file1='default.param'
    file2='autodetect_aper.param'
    file3='default.conv'
    file4='default.nnw'
    command1='cp '+os.path.join(path_files,file1)+' '+path_fits+'/.'
    command2='cp '+os.path.join(path_files,file2)+' '+path_fits+'/.'
    command3='cp '+os.path.join(path_files,file3)+' '+path_fits+'/.'
    command4='cp '+os.path.join(path_files,file4)+' '+path_fits+'/.'
    subprocess.call(shlex.split(command1))
    subprocess.call(shlex.split(command2))
    subprocess.call(shlex.split(command3))
    subprocess.call(shlex.split(command4))

    
